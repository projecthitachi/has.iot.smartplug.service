﻿using Microsoft.AspNet.SignalR.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.smartplug.service
{
    public partial class Service1 : ServiceBase
    {
        #region variable
        private static MqttClient MqttClient_1 = null;
        private static MqttClient MqttClient_2 = null;
        private static string MQTTTopic_1;
        private static string MQTTTopic_2;
        private static MongoClient client = null;
        private static IMongoDatabase database = null;
        static HubConnection connection = null;
        private static IHubProxy appHub = null;
        private static string signalr_server = null;
        private int timesoff = 25330;
        private static Dictionary<string, string> lastZone = new Dictionary<string, string>();
        private static Dictionary<string, int> voTimeH = new Dictionary<string, int>();
        private static Dictionary<string, int> voTimeL = new Dictionary<string, int>();
        private static Dictionary<string, List<decimal>> calculateUseage = new Dictionary<string, List<decimal>>();
        private static Dictionary<string, bool> highUseage = new Dictionary<string, bool>();
        SmartplugAllWidgetRealtime_REC WR = new SmartplugAllWidgetRealtime_REC();
        List<SmartWidget_REC> voDevices = new List<SmartWidget_REC>();
        #endregion

        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                signalr_server = ConfigurationManager.AppSettings["signalr_server"].ToString();
                string mongoDBsvr = ConfigurationManager.AppSettings["mongodb_server"].ToString();
                string MQTTBroker_1 = ConfigurationManager.AppSettings["MQTTBroker_1"].ToString();
                string MQTTBroker_2 = ConfigurationManager.AppSettings["MQTTBroker_2"].ToString();
                MQTTTopic_1 = ConfigurationManager.AppSettings["MQTTTopic_1"].ToString();
                MQTTTopic_2 = ConfigurationManager.AppSettings["MQTTTopic_2"].ToString();
                string MQTTBrokerMultiple = ConfigurationManager.AppSettings["MQTTBrokerMultiple"].ToString();
                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        signalr_server = rootobj["signalr_server"].ToString();
                        mongoDBsvr = rootobj["mongodb_server"].ToString();
                        MQTTBroker_1 = rootobj["MQTTBroker_1"].ToString();
                        MQTTBroker_2 = rootobj["MQTTBroker_2"].ToString();
                        MQTTTopic_1 = rootobj["MQTTTopic_1"].ToString();
                        MQTTTopic_2 = rootobj["MQTTTopic_2"].ToString();
                        MQTTBrokerMultiple = rootobj["MQTTBrokerMultiple"].ToString();
                        timesoff = Convert.ToInt32(rootobj["timesoff"].ToString());
                    }
                }

                initRTHub();

                // mongoDB connection 
                client = new MongoClient(mongoDBsvr);
                database = client.GetDatabase("IoT");

                if (MQTTBrokerMultiple == "true")
                {
                    // mqtt connection device
                    MqttClient_1 = new MqttClient(MQTTBroker_1);
                    MqttClient_1.MqttMsgPublishReceived += client_MqttMsgPublishReceived_1;
                    MqttClient_1.Subscribe(new string[] { MQTTTopic_1 + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                    string clientId_1 = Guid.NewGuid().ToString();
                    MqttClient_1.Connect(clientId_1);

                    void client_MqttMsgPublishReceived_1(object sender, MqttMsgPublishEventArgs e)
                    {
                        var message = System.Text.Encoding.Default.GetString(e.Message);
                        if (IsValidJson(message))
                        {
                            object result = JsonConvert.DeserializeObject(message);
                            JObject voobj = JObject.Parse(result.ToString());

                            string[] topic = e.Topic.Split('/');
                            if (topic.Length > 1)
                            {
                                string Category = topic[0];
                                string hostDevice = topic[1];
                                string IPAddress = topic[2];
                                string cmd = topic[3];
                                switch (cmd)
                                {
                                    case "SENSOR":
                                        SensorHandler(voobj, topic);
                                        break;
                                    case "RESULT":
                                        switchedBtn(voobj, topic);
                                        break;
                                    default:

                                        break;
                                }
                            }
                        }
                    }
                }

                // mqtt connection apps
                MqttClient_2 = new MqttClient(MQTTBroker_2);
                MqttClient_2.MqttMsgPublishReceived += client_MqttMsgPublishReceived_2;
                MqttClient_2.Subscribe(new string[] { MQTTTopic_2 + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                string clientId_2 = Guid.NewGuid().ToString();
                MqttClient_2.Connect(clientId_2);

                void client_MqttMsgPublishReceived_2(object sender, MqttMsgPublishEventArgs e)
                {
                    string[] topic = e.Topic.Split('/');
                    string Category = topic[0];
                    string hostDevice = topic[1];
                    var message = System.Text.Encoding.Default.GetString(e.Message);
                    if (IsValidJson(message))
                    {
                        object result = JsonConvert.DeserializeObject(message);
                        JObject voobj = JObject.Parse(result.ToString());

                        if (topic.Length > 1)
                        {
                            string cmd = topic[3];
                            switch (cmd)
                            {
                                case "SENSOR":
                                    SensorHandler(voobj, topic);
                                    break;
                                case "RESULT":
                                    switchedBtn(voobj, topic);
                                    break;
                                case "SetConfig":
                                    setConfig(message);
                                    break;
                                case "GetConfig":
                                    getConfig(message);
                                    break;
                                default:

                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (topic.Length > 1)
                        {
                            string cmd = topic[3];
                            string IPAddress = topic[2];
                            string msg = message;
                            switch (cmd)
                            {
                                case "cmnd":
                                    if (msg == "ON" || msg == "OFF")
                                    {
                                        if (!calculateUseage.ContainsKey(IPAddress))
                                        {
                                            calculateUseage.Add(IPAddress, new List<decimal>());
                                            highUseage.Add(IPAddress, false);
                                            voTimeL.Add(IPAddress, 0);
                                            voTimeH.Add(IPAddress, 0);
                                        }
                                        calculateUseage[IPAddress].Clear();
                                        highUseage[IPAddress] = false;
                                        voTimeL[IPAddress] = 0;
                                        voTimeH[IPAddress] = 0;
                                    }
                                    if (MQTTBrokerMultiple == "true")
                                    {
                                        MqttClient_1.Publish(e.Topic, e.Message, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                    }
                                    break;
                                default:

                                    break;
                            }
                        }
                    }
                }

                Thread t = new Thread(new ThreadStart(ProcessData));
                Thread tc = new Thread(new ThreadStart(TresHoldConfiguration));
                //Thread p = new Thread(new ThreadStart(Publisher));
                t.Start();
                tc.Start();
                //p.Start();

            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public void ProcessData()
        {
            try
            {
                while (true)
                {
                    var logCollection = database.GetCollection<SmartWidget_REC>("SmartplugDataLogs");
                    var logViewCollection = database.GetCollection<SmartWidget_REC>("SmartplugDataLogs");
                    var deviceCollection = database.GetCollection<SmartDeviceView_REC>("SmartplugDevices");
                    var widgetRealtimeAllColl = database.GetCollection<SmartplugAllWidgetRealtime_REC>("SmartplugAllWidgetRealtime");
                    var widgetRealtimeSpecificColl = database.GetCollection<SmartplugSpecificWidgetRealtime_REC>("SmartplugSpecificWidgetRealtime");


                    int DeviceTotal = 0;
                    int DeviceOn = 0;
                    int DeviceOff = 0;

                    #region collect data consumption
                    var filter = new BsonDocument
                    {
                        {"$gte", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))},
                        {"$lte", DateTime.Now}
                    };
                    string match = "{$match: {RecordTimestamp : " + filter + " }}";
                    string grp = "{$group:{_id:{DeviceID:\"$DeviceID\"},ApparentPower:{$sum:{$toDecimal:\"$ActivePower\"}}}}";
                    var pipeline = logCollection.Aggregate().AppendStage<SmartWidget_REC>(match).AppendStage<SmartWidget_REC>(grp);
                    voDevices = pipeline.ToList();
                    decimal Total = 0;
                    if (voDevices.Count > 0)
                    {
                        foreach (SmartWidget_REC voData in voDevices)
                        {
                            Total += voData.ApparentPower;
                            //var bld1 = Builders<SmartplugSpecificWidgetRealtime_REC>.Filter;
                            //var fltr1 = bld1.Eq<string>("IPAddress", voData._id.DeviceID);
                            //var spec = widgetRealtimeSpecificColl.Find(fltr1).FirstOrDefault();
                            //var update = Builders<SmartplugSpecificWidgetRealtime_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                            //    .Set("TotalToday", voData.ApparentPower / 1000);
                            //widgetRealtimeSpecificColl.UpdateOne(fltr1, update);
                        }
                    }
                    double hours = (DateTime.Now - DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))).TotalHours;
                    //decimal voTotal = (Convert.ToDecimal(Total) * Convert.ToInt32(hours)) / 1000;
                    decimal voTotal = Convert.ToDecimal(Total) / 1000;

                    #region checking status power
                    foreach (SmartDeviceView_REC voData in deviceCollection.Find(_ => true).ToList())
                    {
                        double TotalSeconds = (DateTime.Now - voData.RecordTimestamp).TotalSeconds;
                        if (TotalSeconds > timesoff)
                        {
                            // update Device status
                            var builder = Builders<SmartDeviceView_REC>.Filter;
                            var flt = builder.Eq<string>("IPAddress", voData.IPAddress);
                            var update = Builders<SmartDeviceView_REC>.Update.Set("RecordTimestamp", DateTime.Now).Set("Status", 0);
                            deviceCollection.UpdateOne(flt, update);
                        }
                        DeviceTotal += 1;
                        if (voData.Status == 1) { DeviceOn += 1; } else { DeviceOff += 1; }
                    }
                    #endregion

                    // begin widget all device
                    var bld = Builders<SmartplugAllWidgetRealtime_REC>.Filter;
                    var fltr = bld.Eq<int>("recordID", 1);
                    WR = widgetRealtimeAllColl.Find(fltr).SingleOrDefault();
                    SmartplugAllWidgetRealtime_REC usageTotal = new SmartplugAllWidgetRealtime_REC();
                    usageTotal.recordID = 1;
                    usageTotal.totalUsage = voTotal.ToString("0.##");
                    usageTotal.deviceTotal = DeviceTotal.ToString();
                    usageTotal.deviceOn = DeviceOn.ToString();
                    usageTotal.deviceOff = DeviceOff.ToString();
                    if (WR == null)
                    {
                        widgetRealtimeAllColl.InsertOne(usageTotal);
                    }
                    else
                    {
                        var update = Builders<SmartplugAllWidgetRealtime_REC>.Update.Set("totalUsage", usageTotal.totalUsage)
                            .Set("deviceTotal", usageTotal.deviceTotal)
                            .Set("deviceOn", usageTotal.deviceOn)
                            .Set("deviceOff", usageTotal.deviceOff);
                        widgetRealtimeAllColl.UpdateOne(fltr, update);
                    }
                    // end widget all device

                    // begin widget specific device
                    //var dateDay = DateTime.Now.Day;
                    //var dateDayMonggoDB = DateTime.Now.AddDays(0);
                    //var checkDateDay = "-" + dateDay;
                    //if (checkDateDay != "-0")
                    //{
                    //    dateDayMonggoDB = DateTime.Now.AddDays(Int64.Parse(checkDateDay));
                    //}
                    //// begin widget specific device
                    //var filterm = new BsonDocument
                    //{
                    //    {"$gte", DateTime.Parse(dateDayMonggoDB.ToString("yyyy-MM-dd 00:00:00"))},
                    //    {"$lte", DateTime.Now}
                    //};
                    //string matchm = "{$match: {RecordTimestamp : " + filterm + " }}";
                    //string grpm = "{$group:{_id:{DeviceID:\"$DeviceID\"},ApparentPower:{$sum:{$toDecimal:\"$ActivePower\"}}}}";
                    //var pipelinem = logViewCollection.Aggregate().AppendStage<SmartWidget_REC>(matchm).AppendStage<SmartWidget_REC>(grpm);
                    //var resm = pipelinem.ToList();
                    //if (resm.Count > 0)
                    //{
                    //    foreach (var DeviceData in resm)
                    //    {
                    //        SmartplugSpecificWidgetRealtime_REC specificDev = new SmartplugSpecificWidgetRealtime_REC();
                    //        specificDev.TotalMonth = DeviceData.ApparentPower / 1000;
                    //        //specificDev.TotalToday = voTotal;
                    //        specificDev.MonthlyAvg = specificDev.TotalMonth / 720;
                    //        specificDev.DailyAvg = specificDev.TotalMonth / 24;
                    //        specificDev.IPAddress = DeviceData._id.DeviceID;

                    //        var bld1 = Builders<SmartplugSpecificWidgetRealtime_REC>.Filter;
                    //        var fltr1 = bld1.Eq<string>("IPAddress", DeviceData._id.DeviceID);
                    //        var spec = widgetRealtimeSpecificColl.Find(fltr1).SingleOrDefault();
                    //        if (spec == null)
                    //        {
                    //            widgetRealtimeSpecificColl.InsertOne(specificDev);
                    //        }
                    //        else
                    //        {
                    //            var update = Builders<SmartplugSpecificWidgetRealtime_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                    //            .Set("TotalMonth", specificDev.TotalMonth)
                    //            //.Set("TotalToday", specificDev.TotalToday)
                    //            .Set("MonthlyAvg", specificDev.MonthlyAvg)
                    //            .Set("DailyAvg", specificDev.DailyAvg);
                    //            widgetRealtimeSpecificColl.UpdateOne(fltr1, update);
                    //        }
                    //    }
                    //}
                    // end widget specific device
                    #endregion

                    Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public void TresHoldConfiguration()
        {
            try
            {
                while (true)
                {
                    #region Treshold Enable Configuration
                    var collectionActiveZone = database.GetCollection<SmartlightActiveDevices>("SmartlightActiveDevices");
                    var collectionSmartplugDataLogs = database.GetCollection<SmartplugDataLogs>("SmartplugDataLogs");
                    var Conf2 = database.GetCollection<ConfigurationModel2>("Configuration_VAVPLUG");
                    var SmartDeviceColl = database.GetCollection<SmartDevices_REC>("SmartDevices");
                    var checkTres = Conf2.Find(_ => true).FirstOrDefault();

                    if (checkTres.PLUGStatus == "Enable")
                    {
                        // Active Zone
                        SmartlightActiveDevices z1 = collectionActiveZone.Find(x => x.Instance == 12810).FirstOrDefault();
                        SmartlightActiveDevices z2 = collectionActiveZone.Find(x => x.Instance == 13010).FirstOrDefault();
                        SmartlightActiveDevices z3 = collectionActiveZone.Find(x => x.Instance == 13000).FirstOrDefault();
                        SmartlightActiveDevices z4 = collectionActiveZone.Find(x => x.Instance == 13017).FirstOrDefault();
                        SmartlightActiveDevices z5 = collectionActiveZone.Find(x => x.Instance == 13011).FirstOrDefault();
                        SmartlightActiveDevices z6 = collectionActiveZone.Find(x => x.Instance == 12850).FirstOrDefault();
                        SmartlightActiveDevices z7 = collectionActiveZone.Find(x => x.Instance == 12885).FirstOrDefault();
                        SmartlightActiveDevices z8 = collectionActiveZone.Find(x => x.Instance == 12899).FirstOrDefault();
                        SmartlightActiveDevices z9 = collectionActiveZone.Find(x => x.Instance == 12966).FirstOrDefault();
                        SmartlightActiveDevices z10 = collectionActiveZone.Find(x => x.Instance == 12945).FirstOrDefault();
                        SmartlightActiveDevices z11 = collectionActiveZone.Find(x => x.Instance == 12995).FirstOrDefault();
                        SmartlightActiveDevices z12 = collectionActiveZone.Find(x => x.Instance == 13004).FirstOrDefault();

                        var v_1 = "";
                        var v_2 = "";
                        var v_3 = "";
                        var v_4 = "";
                        var v_5 = "";
                        var v_6 = "";
                        var v_7 = "";
                        var v_8 = "";
                        var v_9 = "";
                        var v_10 = "";
                        var v_11 = "";
                        var v_12 = "";

                        if (z1.PresentValue == "0") { v_1 = "Off"; } else { v_1 = "On"; }
                        if (z2.PresentValue == "0") { v_2 = "Off"; } else { v_2 = "On"; }
                        if (z3.PresentValue == "0") { v_3 = "Off"; } else { v_3 = "On"; }
                        if (z4.PresentValue == "0") { v_4 = "Off"; } else { v_4 = "On"; }
                        if (z5.PresentValue == "0") { v_5 = "Off"; } else { v_5 = "On"; }
                        if (z6.PresentValue == "0") { v_6 = "Off"; } else { v_6 = "On"; }
                        if (z7.PresentValue == "0") { v_7 = "Off"; } else { v_7 = "On"; }
                        if (z8.PresentValue == "0") { v_8 = "Off"; } else { v_8 = "On"; }
                        if (z9.PresentValue == "0") { v_9 = "Off"; } else { v_9 = "On"; }
                        if (z10.PresentValue == "0") { v_10 = "Off"; } else { v_10 = "On"; }
                        if (z11.PresentValue == "0") { v_11 = "Off"; } else { v_11 = "On"; }
                        if (z12.PresentValue == "0") { v_12 = "Off"; } else { v_12 = "On"; }

                        var res_conf = Conf2.Find(_ => true).FirstOrDefault();
                        var PlugStatus = res_conf.PLUGStatus;
                        var PlugLow = res_conf.PLUGLow;
                        var PlugMedium = res_conf.PLUGMedium;
                        var PlugHigh = res_conf.PLUGHigh;
                        var CurrentStatus = "";

                        ((PlugStatus == "Low") ? (Action)(() => { CurrentStatus = PlugLow; }) : () => { })();
                        ((PlugStatus == "Medium") ? (Action)(() => { CurrentStatus = PlugMedium; }) : () => { })();
                        ((PlugStatus == "High") ? (Action)(() => { CurrentStatus = PlugHigh; }) : () => { })();
                        double CS = Convert.ToDouble(CurrentStatus);

                        var builder2 = Builders<SmartDevices_REC>.Filter;
                        var up1 = Builders<SmartDevices_REC>.Update.Set("Relay", "0");
                        var up_on = Builders<SmartDevices_REC>.Update.Set("Relay", "1");

                        if (v_11 == "On")
                        {
                            var res_49 = collectionSmartplugDataLogs.Find(f => f.IPAddress == "10.0.1.49").Limit(1).SingleOrDefault();
                            var res_50 = collectionSmartplugDataLogs.Find(f => f.IPAddress == "10.0.1.50").Limit(1).SingleOrDefault();
                            double AP_49 = Convert.ToDouble(res_49.ApparentPower);
                            double AP_50 = Convert.ToDouble(res_50.ApparentPower);

                            if (AP_49 > CS)
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.49/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt4 = builder2.Eq<string>("IPAddress", "10.0.1.49");
                                SmartDeviceColl.UpdateOne(flt4, up1);
                            }
                            else
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.49/cmnd/POWER", Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt4 = builder2.Eq<string>("IPAddress", "10.0.1.49");
                                SmartDeviceColl.UpdateOne(flt4, up_on);
                            }
                            if (AP_50 > CS)
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.50/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt5 = builder2.Eq<string>("IPAddress", "10.0.1.50");
                                SmartDeviceColl.UpdateOne(flt5, up1);
                            }
                            else
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.50/cmnd/POWER", Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt5 = builder2.Eq<string>("IPAddress", "10.0.1.50");
                                SmartDeviceColl.UpdateOne(flt5, up_on);
                            }
                        }
                        else
                        {
                            MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.49/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                            MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.50/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                            var flt4 = builder2.Eq<string>("IPAddress", "10.0.1.49");
                            var flt5 = builder2.Eq<string>("IPAddress", "10.0.1.50");

                            SmartDeviceColl.UpdateOne(flt4, up1);
                            SmartDeviceColl.UpdateOne(flt5, up1);
                        }
                        if (v_10 == "On")
                        {
                            var res_46 = collectionSmartplugDataLogs.Find(f => f.IPAddress == "10.0.1.46").Limit(1).SingleOrDefault();
                            var res_47 = collectionSmartplugDataLogs.Find(f => f.IPAddress == "10.0.1.47").Limit(1).SingleOrDefault();
                            var res_48 = collectionSmartplugDataLogs.Find(f => f.IPAddress == "10.0.1.48").Limit(1).SingleOrDefault();
                            double AP_46 = Convert.ToDouble(res_46.ApparentPower);
                            double AP_47 = Convert.ToDouble(res_47.ApparentPower);
                            double AP_48 = Convert.ToDouble(res_48.ApparentPower);

                            if (AP_46 > CS)
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.46/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt1 = builder2.Eq<string>("IPAddress", "10.0.1.46");
                                SmartDeviceColl.UpdateOne(flt1, up1);
                            }
                            else
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.46/cmnd/POWER", Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt1 = builder2.Eq<string>("IPAddress", "10.0.1.46");
                                SmartDeviceColl.UpdateOne(flt1, up_on);
                            }
                            if (AP_47 > CS)
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.47/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt2 = builder2.Eq<string>("IPAddress", "10.0.1.47");
                                SmartDeviceColl.UpdateOne(flt2, up1);
                            }
                            else
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.47/cmnd/POWER", Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt2 = builder2.Eq<string>("IPAddress", "10.0.1.47");
                                SmartDeviceColl.UpdateOne(flt2, up_on);
                            }
                            if (AP_48 > CS)
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.48/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt3 = builder2.Eq<string>("IPAddress", "10.0.1.48");
                                SmartDeviceColl.UpdateOne(flt3, up1);
                            }
                            else
                            {
                                MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.48/cmnd/POWER", Encoding.UTF8.GetBytes("ON"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                var flt3 = builder2.Eq<string>("IPAddress", "10.0.1.48");
                                SmartDeviceColl.UpdateOne(flt3, up_on);
                            }
                        }
                        else
                        {
                            MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.46/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                            MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.47/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                            MqttClient_2.Publish("Smartplug/Tasmota/10.0.1.48/cmnd/POWER", Encoding.UTF8.GetBytes("OFF"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                            var flt1 = builder2.Eq<string>("IPAddress", "10.0.1.46");
                            var flt2 = builder2.Eq<string>("IPAddress", "10.0.1.47");
                            var flt3 = builder2.Eq<string>("IPAddress", "10.0.1.48");

                            SmartDeviceColl.UpdateOne(flt1, up1);
                            SmartDeviceColl.UpdateOne(flt2, up1);
                            SmartDeviceColl.UpdateOne(flt3, up1);
                        }
                    }

                    #endregion

                    Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public void Publisher()
        {
            var levelUsage = database.GetCollection<SmartPlugLevelUseage_REC>("SmartPlugLevelUsage");
            var PersonLoc = database.GetCollection<PersonLocation>("PersonLocation");
            while (true)
            {
                #region publish main widget smartplug
                Dictionary<string, string> ResultData = new Dictionary<string, string>();
                if (WR != null)
                {
                    ResultData.Add("Total", WR.totalUsage);
                    ResultData.Add("TotalDevice", WR.deviceTotal);
                    ResultData.Add("DeviceOn", WR.deviceOn);
                    ResultData.Add("DeviceOff", WR.deviceOff);
                }
                else
                {
                    ResultData.Add("Total", "0");
                    ResultData.Add("TotalDevice", "0");
                    ResultData.Add("DeviceOn", "0");
                    ResultData.Add("DeviceOff", "0");
                }
                string sv = Convert.ToString(JsonConvert.SerializeObject(ResultData));
                heandlerSignalR("WidgetRealtime", "Smartplug", sv);
                //MqttClient_2.Publish("WidgetRealtime/Smartplug", Encoding.UTF8.GetBytes(sv), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                #endregion

                foreach (SmartPlugLevelUseage_REC voLevel in levelUsage.Find(x => x.category == "Low").ToList())
                {
                    string IPAddress = voLevel.ip;
                    var blds = Builders<PersonLocation>.Filter;
                    var fltrs = blds.Eq<string>("personID", voLevel.personId);
                    var PRS = PersonLoc.Find(fltrs).SingleOrDefault();
                    if (PRS != null)
                    {

                        if (!lastZone.ContainsKey(voLevel.ip))
                        {
                            lastZone.Add(voLevel.ip, PRS.location);
                        }
                        if (PRS.location != lastZone[voLevel.ip])
                        {
                            voTimeH[voLevel.ip] = 0;
                            calculateUseage[IPAddress].Clear();
                            highUseage[voLevel.ip] = false;
                            voTimeL[voLevel.ip] = 0;
                        }
                        lastZone[voLevel.ip] = PRS.location;
                    }
                }
                Thread.Sleep(1000);
            }
        }
        public static void SensorHandler(JObject voobj, string[] topic)
        {
            try
            {
                #region loging and checking gadget usage
                if (voobj.ContainsKey("ENERGY"))
                {
                    var DataLogs = database.GetCollection<SmartLog_REC>("SmartplugDataLogs");
                    var collection = database.GetCollection<SmartWidget_REC>("SmartplugDataLogs");
                    var InserDevice = database.GetCollection<SmartDevice_REC>("SmartplugDevices");
                    var CollectDevice = database.GetCollection<SmartDeviceView_REC>("SmartplugDevices");
                    var levelUsage = database.GetCollection<SmartPlugLevelUseage_REC>("SmartPlugLevelUsage");
                    var widgetRealtimeSpecificColl = database.GetCollection<SmartplugSpecificWidgetRealtime_REC>("SmartplugSpecificWidgetRealtime");
                    var PersonLoc = database.GetCollection<PersonLocation>("PersonLocation");

                    object resultEnergi = JsonConvert.DeserializeObject(voobj["ENERGY"].ToString());
                    JObject obj = JObject.Parse(resultEnergi.ToString());
                    #region Insert Data Log
                    string DeviceID = topic[2];
                    string Voltage = (obj["Voltage"] == null) ? "0" : obj["Voltage"].ToString().ToString();
                    string Current = (obj["Current"] == null) ? "0" : obj["Current"].ToString();
                    string Power = (obj["Power"] == null) ? "0" : obj["Power"].ToString();
                    string Apparent = (obj["ApparentPower"] == null) ? "0" : obj["ApparentPower"].ToString();
                    string Factor = (obj["Factors"] == null) ? "0" : obj["Factors"].ToString();
                    string Energy = (obj["energy"] == null) ? "0" : obj["energy"].ToString();
                    string Host = (obj["host"] == null) ? "" : obj["host"].ToString();
                    string IP = (obj["ip"] == null) ? DeviceID : obj["ip"].ToString();
                    string MAC = (obj["mac"] == null) ? "" : obj["mac"].ToString();
                    // insert data log
                    SmartLog_REC voData = new SmartLog_REC();
                    voData.RecordTimestamp = DateTime.Now;
                    voData.DeviceID = DeviceID;
                    voData.IPAddress = IP;
                    voData.Voltage = Convert.ToInt32(Voltage);
                    voData.ActivePower = Convert.ToDecimal(Power);
                    voData.ApparentPower = Convert.ToDecimal(Apparent);
                    voData.PowerFactor = Convert.ToInt32(Factor);
                    voData.Energy = Convert.ToInt32(Energy);
                    voData.Current = Convert.ToDecimal(Current);
                    DataLogs.InsertOne(voData);

                    SmartChart_REC poData = new SmartChart_REC();
                    poData.RecordTimestamp = DateTime.Now;
                    poData.DeviceID = DeviceID;
                    poData.IPAddress = IP;
                    poData.Voltage = Convert.ToInt32(Voltage);
                    poData.ActivePower = Convert.ToDecimal(Power);
                    poData.ApparentPower = Convert.ToDecimal(Apparent);
                    poData.PowerFactor = Convert.ToInt32(Factor);
                    poData.Energy = Convert.ToInt32(Energy);
                    poData.Current = Convert.ToDecimal(Current);
                    #endregion

                    #region check gadget
                    string pathroot = AppDomain.CurrentDomain.BaseDirectory + "power_consumption_gadget.json";
                    JObject objHp = null;
                    JObject objlp = null;
                    JObject objunk = null;
                    if (System.IO.File.Exists(pathroot))
                    {
                        string path = System.IO.Directory.GetCurrentDirectory();
                        string file = pathroot;

                        string Json = System.IO.File.ReadAllText(file);
                        object rootresult = Newtonsoft.Json.JsonConvert.DeserializeObject(Json);
                        Newtonsoft.Json.Linq.JObject rootobj = Newtonsoft.Json.Linq.JObject.Parse(rootresult.ToString());

                        object resulthp = Newtonsoft.Json.JsonConvert.DeserializeObject(rootobj["handphone"].ToString());
                        objHp = JObject.Parse(resulthp.ToString());

                        object resultlp = Newtonsoft.Json.JsonConvert.DeserializeObject(rootobj["laptop"].ToString());
                        objlp = JObject.Parse(resultlp.ToString());

                        object resultunk = Newtonsoft.Json.JsonConvert.DeserializeObject(rootobj["unknown"].ToString());
                        objunk = JObject.Parse(resultunk.ToString());
                    }
                    if (voData.ActivePower <= Convert.ToInt32(objunk["max_oncharging"].ToString()) && voData.ActivePower >= Convert.ToInt32(objunk["min_oncharging"].ToString()))
                    {
                        if (voData.ActivePower <= Convert.ToInt32(objunk["max_fullcharging"].ToString()) && voData.ActivePower >= Convert.ToInt32(objunk["min_fullcharging"].ToString()))
                        {
                            poData.fullcharger = "yes";
                        }
                        else
                        {
                            poData.fullcharger = "no";
                        }
                        poData.GadGet = "unknown";
                    }
                    else if (voData.ActivePower <= Convert.ToInt32(objlp["max_oncharging"].ToString()) && voData.ActivePower >= Convert.ToInt32(objlp["min_oncharging"].ToString()))
                    {
                        if (voData.ActivePower <= Convert.ToInt32(objlp["max_fullcharging"].ToString()) && voData.ActivePower >= Convert.ToInt32(objlp["min_fullcharging"].ToString()))
                        {
                            poData.fullcharger = "yes";
                        }
                        else
                        {
                            poData.fullcharger = "no";
                        }
                        poData.GadGet = "laptop";
                    }
                    else if (voData.ActivePower <= Convert.ToInt32(objHp["max_oncharging"].ToString()) && voData.ActivePower >= Convert.ToInt32(objHp["min_oncharging"].ToString()))
                    {
                        if (voData.ActivePower <= Convert.ToInt32(objHp["max_fullcharging"].ToString()) && voData.ActivePower >= Convert.ToInt32(objHp["min_fullcharging"].ToString()))
                        {
                            poData.fullcharger = "yes";
                        }
                        else
                        {
                            poData.fullcharger = "no";
                        }
                        poData.GadGet = "handphone";
                    }
                    else if (voData.ActivePower == 0)
                    {
                        poData.fullcharger = "poweroff";
                        poData.GadGet = "poweroff";
                    }
                    #endregion

                    #region Spesific Widget
                    var bldt = Builders<SmartplugSpecificWidgetRealtime_REC>.Filter;
                    var fltt = bldt.Eq<string>("IPAddress", poData.IPAddress);
                    var WidgetSpecific = widgetRealtimeSpecificColl.Find(fltt).SingleOrDefault();
                    if (WidgetSpecific != null)
                    {
                        poData.TotalMonth = WidgetSpecific.TotalMonth;
                        poData.TotalToday = WidgetSpecific.TotalToday;
                        poData.MonthlyAvg = WidgetSpecific.MonthlyAvg;
                        poData.DailyAvg = WidgetSpecific.DailyAvg;
                    }
                    else
                    {
                        poData.TotalMonth = Convert.ToDecimal(0.00);
                        poData.TotalToday = Convert.ToDecimal(0.00);
                        poData.MonthlyAvg = Convert.ToDecimal(0.00);
                        poData.DailyAvg = Convert.ToDecimal(0.00);
                    }
                    string sp = Convert.ToString(JsonConvert.SerializeObject(poData));
                    heandlerSignalR("WidgetRealtime", "SmartplugDevice", sp);
                    //MqttClient_2.Publish("WidgetRealtime/SmartplugDevice", Encoding.UTF8.GetBytes(sp), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                    foreach (SmartPlugLevelUseage_REC voLevel in levelUsage.Find(x => x.ip == poData.IPAddress && x.category == "Low").ToList())
                    {
                        if (!calculateUseage.ContainsKey(poData.IPAddress))
                        {
                            calculateUseage.Add(poData.IPAddress, new List<decimal>());
                            highUseage.Add(poData.IPAddress, false);
                            voTimeL.Add(poData.IPAddress, 0);
                            voTimeH.Add(poData.IPAddress, 0);
                        }

                        var blds = Builders<PersonLocation>.Filter;
                        var fltrs = blds.Eq<string>("personID", voLevel.personId);
                        var PRS = PersonLoc.Find(fltrs).SingleOrDefault();

                        if (voLevel.min <= poData.ActivePower && voLevel.max >= poData.ActivePower && highUseage[poData.IPAddress] == false) { highUseage[poData.IPAddress] = true; }
                        //if usage in range min
                        if (highUseage[poData.IPAddress])
                        {
                            calculateUseage[poData.IPAddress].Add(poData.ActivePower); // collect active usage
                            voTimeH[poData.IPAddress]++;
                            if (voTimeH[poData.IPAddress] > 9)
                            {
                                decimal avgUsages = Convert.ToDecimal(0.00);
                                foreach (decimal voUsage in calculateUseage[poData.IPAddress])
                                {
                                    avgUsages += voUsage;
                                }
                                decimal avgUsage = avgUsages / calculateUseage[poData.IPAddress].Count;

                                if (PRS != null)
                                {
                                    if (PRS.location != PRS.defaultZone && voLevel.min <= avgUsage && voLevel.max >= avgUsage) // if person not in location and avg usage still in range min
                                    {
                                        string strValue = Convert.ToString("OFF");
                                        MqttClient_2.Publish("Smartplug/Tasmota/" + DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                    }
                                }
                                else
                                {
                                    string strValue = Convert.ToString("OFF");
                                    MqttClient_2.Publish("Smartplug/Tasmota/" + DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                }
                                voTimeH[poData.IPAddress] = 0;
                                calculateUseage[poData.IPAddress].Clear();
                                highUseage[poData.IPAddress] = false;
                                voTimeL[poData.IPAddress] = 0;

                                var bd = Builders<SmartPlugLevelUseage_REC>.Filter;
                                var fltr = bd.Eq<int>("recordID", voLevel.recordID);
                                var updt = Builders<SmartPlugLevelUseage_REC>.Update.Set("value", 1);
                                levelUsage.UpdateOne(fltr, updt);
                            }
                        } // if usage not in range min
                        else
                        {
                            //voTimeL[poData.IPAddress]++;
                            //if (voTimeL[poData.IPAddress] > 9)
                            //{
                            //    if (PRS != null)
                            //    {
                            //        if (PRS.location != PRS.defaultZone) // if person not in location
                            //        {
                            //            string strValue = Convert.ToString("OFF");
                            //            MqttClient_2.Publish("Smartplug/Tasmota/" + DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        string strValue = Convert.ToString("OFF");
                            //        MqttClient_2.Publish("Smartplug/Tasmota/" + DeviceID + "/cmnd/POWER", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                            //    }
                            //    voTimeL[poData.IPAddress] = 0;
                            //}
                            var bd = Builders<SmartPlugLevelUseage_REC>.Filter;
                            var fltr = bd.Eq<int>("recordID", voLevel.recordID);
                            var updt = Builders<SmartPlugLevelUseage_REC>.Update.Set("value", 0);
                            levelUsage.UpdateOne(fltr, updt);
                        }
                    }
                    #endregion

                    #region hearbeat device
                    // hearbeat device
                    var builder = Builders<SmartDeviceView_REC>.Filter;
                    var flt = builder.Eq<string>("IPAddress", IP);
                    var resData = CollectDevice.Find(flt).SingleOrDefault();
                    if (resData == null)
                    {
                        SmartDevice_REC voDevice = new SmartDevice_REC();
                        voDevice.RecordTimestamp = DateTime.Now;
                        voDevice.DeviceID = DeviceID;
                        voDevice.IPAddress = IP;
                        voDevice.Status = 1;
                        voDevice.Relay = 1;
                        InserDevice.InsertOne(voDevice);
                    }
                    else
                    {
                        var update = Builders<SmartDeviceView_REC>.Update.Set("RecordTimestamp", DateTime.Now).Set("Status", 1);
                        CollectDevice.UpdateOne(flt, update);
                    }
                    #endregion

                    #region Collect data Realtime Widget
                    //Count total Data
                    var deviceCollectionData = database.GetCollection<SmartDeviceDataView_REC>("SmartDevices");
                    var logCollectionData = database.GetCollection<SmartPlugLogView_REC>("SmartplugDataLogs");
                    var widgetCollectionData = database.GetCollection<SmartplugDataWidgetRealtime_REC>("SmartplugWidgetRealtime");
                    List<SmartDataWidget_REC> voDevicesData = new List<SmartDataWidget_REC>();

                    var Minute = DateTime.Now.Minute;
                    var datetimeMonggoDB = DateTime.Now.AddMinutes(0);
                    var checkMinute = "-" + Minute;
                    if (checkMinute != "-0")
                    {
                        datetimeMonggoDB = DateTime.Now.AddMinutes(Int64.Parse(checkMinute));
                    }

                    var bldLog = Builders<SmartPlugLogView_REC>.Filter;
                    var fltLog = bldLog.Eq<string>("IPAddress", voData.IPAddress);
                    fltLog &= bldLog.Gte<DateTime>("RecordTimestamp", datetimeMonggoDB);
                    var resDataLogs = logCollectionData.Find(fltLog).SortBy(x => x.RecordTimestamp).ToList();
                    double lastValCons = 0;
                    double lastPower = 0;
                    double TotalDataCons = 0;
                    double convertDataPower = 0;
                    double dataPowerMin1 = 0;
                    double dataPowerPlus1 = 0;
                    if (resDataLogs != null)
                    {
                        foreach (SmartPlugLogView_REC voDataDevices in resDataLogs)
                        {
                            SmartplugDataWidgetRealtime_REC poWidget = new SmartplugDataWidgetRealtime_REC();
                            double dataPower = Decimal.ToDouble(voDataDevices.ActivePower);
                            dataPowerMin1 = dataPower - 1;
                            dataPowerPlus1 = dataPower + 1;
                            convertDataPower = dataPower / 1000;
                            TotalDataCons = lastPower + convertDataPower;

                            var builder1 = Builders<SmartDeviceDataView_REC>.Filter;
                            var flt1 = builder1.Eq<string>("IPAddress", voDataDevices.IPAddress);
                            var resDevice = deviceCollectionData.Find(flt1).FirstOrDefault();

                            var builder2 = Builders<SmartplugDataWidgetRealtime_REC>.Filter;
                            var flt2 = builder2.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00")));
                            flt2 &= builder2.Eq<string>("IPAddress", resDevice.IPAddress);
                            var resData2 = widgetCollectionData.Find(flt2).FirstOrDefault();
                            DateTime dateNow = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                            if (resData2 == null)
                            {
                                poWidget.RecordTimestamp = dateNow;
                                poWidget.DeviceID = resDevice.DeviceID;
                                poWidget.DeviceName = resDevice.DeviceName;
                                poWidget.IPAddress = resDevice.IPAddress;
                                poWidget.Location = resDevice.Location;
                                poWidget.Consumption = Convert.ToDouble(TotalDataCons.ToString("0.####"));
                                widgetCollectionData.InsertOne(poWidget);
                            }
                            else if (dataPower != lastValCons && dataPowerMin1 != lastValCons && dataPowerPlus1 != lastValCons)
                            {
                                var poWidgetUpdate = Builders<SmartplugDataWidgetRealtime_REC>.Update.Set("Consumption", Convert.ToDouble(TotalDataCons.ToString("0.####")));
                                widgetCollectionData.UpdateOne(flt2, poWidgetUpdate);
                            }

                            if (dataPower != lastValCons)
                            {
                                lastPower = TotalDataCons;
                            }
                            lastValCons = dataPower;
                        }
                    }
                    #endregion

                    #region Collect data Widget a Day
                    var widgetCollectionDataDay = database.GetCollection<SmartplugDataWidget_REC>("SmartplugWidget");
                    var widgetCollectionDataWeek = database.GetCollection<SmartplugWeekdayConsumtionAvg_REC>("SmartplugWeekdayConsumtionAvg");

                    var bldLogDay = Builders<SmartplugDataWidgetRealtime_REC>.Filter;
                    var fltLogDay = bldLogDay.Eq<string>("IPAddress", voData.IPAddress);
                    fltLogDay &= bldLogDay.Gte<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00")));
                    var resDataLogsDay = widgetCollectionData.Find(fltLogDay).SortBy(x => x.RecordTimestamp).ToList();
                    double lastPowerDay = 0;
                    double TotalDataDay = 0;
                    int totalDataDay = resDataLogsDay.Count;
                    if (resDataLogsDay != null)
                    {
                        foreach (SmartplugDataWidgetRealtime_REC voDataDevicesDay in resDataLogsDay)
                        {
                            SmartplugDataWidget_REC poWidgetDays = new SmartplugDataWidget_REC();
                            double dataPower = voDataDevicesDay.Consumption;
                            TotalDataDay = lastPowerDay + dataPower;

                            var builder1 = Builders<SmartDeviceDataView_REC>.Filter;
                            var flt1 = builder1.Eq<string>("IPAddress", voDataDevicesDay.IPAddress);
                            var resDevice = deviceCollectionData.Find(flt1).FirstOrDefault();

                            var builderDays = Builders<SmartplugDataWidget_REC>.Filter;
                            var fltDays = builderDays.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00")));
                            fltDays &= builderDays.Eq<string>("IPAddress", resDevice.IPAddress);
                            var resDataDays = widgetCollectionDataDay.Find(fltDays).FirstOrDefault();
                            DateTime dateNow = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
                            if (resDataDays == null)
                            {
                                poWidgetDays.RecordTimestamp = dateNow;
                                poWidgetDays.DeviceID = resDevice.DeviceID;
                                poWidgetDays.DeviceName = resDevice.DeviceName;
                                poWidgetDays.IPAddress = resDevice.IPAddress;
                                poWidgetDays.Location = resDevice.Location;
                                poWidgetDays.Consumption = Convert.ToDouble(TotalDataDay.ToString("0.####"));
                                widgetCollectionDataDay.InsertOne(poWidgetDays);
                            }
                            else
                            {
                                var poWidgetDaysUpdate = Builders<SmartplugDataWidget_REC>.Update.Set("Consumption", Convert.ToDouble(TotalDataDay.ToString("0.####")));
                                widgetCollectionDataDay.UpdateOne(fltDays, poWidgetDaysUpdate);
                            }
                            lastPowerDay = TotalDataDay;
                        }
                        //#region WeekDaySmartplug
                        //SmartplugWeekdayConsumtionAvg_REC poWidgetWeek = new SmartplugWeekdayConsumtionAvg_REC();

                        //var DayAvg = DateTime.Now.DayOfWeek;
                        //var NameDay = DayAvg.ToString().Substring(0, 3);
                        //if (NameDay == "Mon" || NameDay == "Tue" || NameDay == "Wed" || NameDay == "Thu" || NameDay == "Fri")
                        //{
                        //    var convertPower = lastPowerDay * 1000;
                        //    var TotalDataPowerAvg = convertPower / 24;

                        //    var builderWeek = Builders<SmartplugWeekdayConsumtionAvg_REC>.Filter;
                        //    var fltWeek = builderWeek.Eq<string>("day", NameDay);
                        //    fltWeek &= builderWeek.Eq<string>("ip", voData.IPAddress);
                        //    var resDataWeek = widgetCollectionDataWeek.Find(fltWeek).FirstOrDefault();
                        //    DateTime dateNowWeek = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
                        //    if (resDataWeek == null)
                        //    {
                        //        poWidgetWeek.recordTimestamp = dateNowWeek;
                        //        poWidgetWeek.day = NameDay;
                        //        poWidgetWeek.unit = "Watt";
                        //        poWidgetWeek.Current = (int)Math.Floor(TotalDataPowerAvg);
                        //        poWidgetWeek.Baseline = (int)Math.Floor(TotalDataPowerAvg) + 10;
                        //        poWidgetWeek.ip = voData.IPAddress;
                        //        widgetCollectionDataWeek.InsertOne(poWidgetWeek);
                        //    }
                        //    else
                        //    {
                        //        var poWidgetWeekUpdate = Builders<SmartplugWeekdayConsumtionAvg_REC>.Update
                        //            .Set("recordTimestamp", DateTime.Now)
                        //            .Set("Current", (int)Math.Floor(TotalDataPowerAvg))
                        //            .Set("Baseline", (int)Math.Floor(TotalDataPowerAvg) + 10);
                        //        widgetCollectionDataWeek.UpdateOne(fltWeek, poWidgetWeekUpdate);
                        //    }
                        //}
                        //#endregion

                        #region Total Today Power
                        var bld1 = Builders<SmartplugSpecificWidgetRealtime_REC>.Filter;
                        var fltr1 = bld1.Eq<string>("IPAddress", voData.IPAddress);
                        widgetRealtimeSpecificColl.Find(fltr1).SingleOrDefault();

                        var update = Builders<SmartplugSpecificWidgetRealtime_REC>.Update.Set("TotalToday", lastPowerDay);
                        widgetRealtimeSpecificColl.UpdateOne(fltr1, update);
                        #endregion
                    }
                    #endregion

                    #region WeekDaySmartplug
                    SmartplugWeekdayConsumtionAvg_REC poWidgetWeek = new SmartplugWeekdayConsumtionAvg_REC();

                    var DayAvg = DateTime.Now.DayOfWeek;
                    var NameDay = DayAvg.ToString().Substring(0, 3);
                    if (NameDay == "Mon" || NameDay == "Tue" || NameDay == "Wed" || NameDay == "Thu" || NameDay == "Fri")
                    {
                        var bsonDocument = new BsonDocument();
                        bsonDocument["gte"] = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
                        bsonDocument["lte"] = DateTime.Now;
                        string matchm = "{$match: {$and: [{ RecordTimestamp: {$gte: ISODate(\"" + bsonDocument["gte"] + "\"),$lte: ISODate(\"" + bsonDocument["lte"] + "\")}},{DeviceID:\"" + voData.IPAddress + "\"}]}}";
                        string grpm = "{$group: { _id: { DeviceID: \"$DeviceID\" }, ApparentPower: { $sum: { $toDecimal: \"$ActivePower\" }}, count:{$sum:1}}}";
                        var pipelinem = logCollectionData.Aggregate().AppendStage<SmartWidgetLog_REC>(matchm).AppendStage<SmartWidgetLog_REC>(grpm);
                        var resm = pipelinem.FirstOrDefault();
                        if (resm != null)
                        {
                            var TotalDataPowerAvg = resm.ApparentPower / resm.count;

                            var builderWeek = Builders<SmartplugWeekdayConsumtionAvg_REC>.Filter;
                            var fltWeek = builderWeek.Eq<string>("day", NameDay);
                            fltWeek &= builderWeek.Eq<string>("ip", voData.IPAddress);
                            var resDataWeek = widgetCollectionDataWeek.Find(fltWeek).FirstOrDefault();
                            DateTime dateNowWeek = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
                            if (resDataWeek == null)
                            {
                                poWidgetWeek.recordTimestamp = dateNowWeek;
                                poWidgetWeek.day = NameDay;
                                poWidgetWeek.unit = "Watt";
                                poWidgetWeek.Current = (int)Math.Floor(TotalDataPowerAvg);
                                poWidgetWeek.Baseline = (int)Math.Floor(TotalDataPowerAvg) + 10;
                                poWidgetWeek.ip = voData.IPAddress;
                                widgetCollectionDataWeek.InsertOne(poWidgetWeek);
                            }
                            else
                            {
                                var poWidgetWeekUpdate = Builders<SmartplugWeekdayConsumtionAvg_REC>.Update
                                    .Set("recordTimestamp", DateTime.Now)
                                    .Set("Current", (int)Math.Floor(TotalDataPowerAvg))
                                    .Set("Baseline", (int)Math.Floor(TotalDataPowerAvg) + 10);
                                widgetCollectionDataWeek.UpdateOne(fltWeek, poWidgetWeekUpdate);
                            }
                        }
                    }
                    #endregion

                    #region Collect data Widget a Month
                    var dateDay = DateTime.Now.Day;
                    var dateMonth = DateTime.Now.Month;
                    var dateDayMonggoDB = DateTime.Now.AddDays(0);
                    var dateMonthMonggoDB = DateTime.Now.AddMonths(0);
                    var checkDateDay = "-" + dateDay;
                    var checkMonth = "-" + dateMonth;
                    if (checkDateDay != "-0")
                    {
                        dateDayMonggoDB = DateTime.Now.AddMonths((int)Int64.Parse(checkMonth)).AddDays(Int64.Parse(checkDateDay)).AddHours(23).AddMinutes(59).AddSeconds(59);
                    }
                    var bldLogMonth = Builders<SmartplugDataWidgetRealtime_REC>.Filter;
                    var fltLogMonth = bldLogMonth.Eq<string>("IPAddress", voData.IPAddress);
                    fltLogMonth &= bldLogMonth.Gt<DateTime>("RecordTimestamp", dateDayMonggoDB);
                    var resDataLogsMonth = widgetCollectionData.Find(fltLogMonth).SortBy(x => x.RecordTimestamp).ToList();
                    var totalData = resDataLogsMonth.Count;
                    decimal TotalMonth = 0;
                    decimal MonthlyAvg = 0;
                    decimal DailyAvg = 0;
                    decimal LastTotalMonth = 0;
                    decimal LastMonthlyAvg = 0;
                    decimal LastDailyAvg = 0;
                    if (resDataLogsMonth != null)
                    {
                        foreach (var DeviceData in resDataLogsMonth)
                        {
                            SmartplugSpecificWidgetRealtime_REC specificDev = new SmartplugSpecificWidgetRealtime_REC();
                            specificDev.TotalMonth = (decimal)DeviceData.Consumption;
                            specificDev.MonthlyAvg = specificDev.TotalMonth / totalData;
                            specificDev.DailyAvg = specificDev.TotalMonth / 24;
                            specificDev.IPAddress = DeviceData.IPAddress;

                            TotalMonth = (decimal)DeviceData.Consumption + LastTotalMonth;
                            MonthlyAvg = (TotalMonth / totalData) + LastMonthlyAvg;
                            DailyAvg = (TotalMonth / totalData / 24) + LastDailyAvg;

                            var bld1 = Builders<SmartplugSpecificWidgetRealtime_REC>.Filter;
                            var fltr1 = bld1.Eq<string>("IPAddress", DeviceData.IPAddress);
                            var spec = widgetRealtimeSpecificColl.Find(fltr1).SingleOrDefault();
                            if (spec == null)
                            {
                                widgetRealtimeSpecificColl.InsertOne(specificDev);
                            }
                            else
                            {
                                var update = Builders<SmartplugSpecificWidgetRealtime_REC>.Update.Set("RecordTimestamp", DateTime.Now)
                                .Set("TotalMonth", TotalMonth)
                                .Set("MonthlyAvg", MonthlyAvg)
                                .Set("DailyAvg", DailyAvg);
                                widgetRealtimeSpecificColl.UpdateOne(fltr1, update);
                            }

                            LastTotalMonth = TotalMonth;
                            LastMonthlyAvg = MonthlyAvg;
                            LastDailyAvg = DailyAvg;
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public static void switchedBtn(JObject voobj, string[] topic)
        {
            try
            {
                if (voobj.ContainsKey("POWER1"))
                {
                    SwitchRelay voData = new SwitchRelay();
                    voData.Status = voobj["POWER1"].ToString();
                    voData.IPAddress = topic[2].ToString();
                    // begin switch relay
                    string sp = Convert.ToString(JsonConvert.SerializeObject(voData));
                    heandlerSignalR("WidgetRealtime", "SmartplugSwitch", sp);
                    //MqttClient_2.Publish("WidgetRealtime/SmartplugSwitch", Encoding.UTF8.GetBytes(sp), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    // end switch relay
                    //var SmartDeviceColl = database.GetCollection<SmartDevices_REC>("SmartDevices");
                    //var CollectDevice = database.GetCollection<SmartDevicesView_REC>("SmartDevices");
                    //var switchedOff = database.GetCollection<SmartplugSwitchedOff_REC>("SmartplugSwitchedOff");

                    //var bld1 = Builders<SmartDevices_REC>.Filter;
                    //var flt1 = bld1.Eq<string>("IPAddress", voData.IPAddress);

                    //var status = 0;
                    //var statusInsert = 0;
                    //if (voData.Status == "1" || voData.Status == "ON")
                    //{
                    //    #region insert chart switched On
                    //    var builder2 = Builders<SmartDevicesView_REC>.Filter;
                    //    var flt2 = builder2.Eq<string>("IPAddress", voData.IPAddress);
                    //    var resDevice = CollectDevice.Find(flt2).SingleOrDefault();

                    //    var statusRelay = resDevice.Relay == 1 ? "ON" : "OFF";
                    //    if (statusRelay != voData.Status)
                    //    {
                    //        if (voData.Status == "ON")
                    //        {
                    //            status = 1;
                    //        }
                    //        var up1 = Builders<SmartDevices_REC>.Update.Set("Relay", status);
                    //        SmartDeviceColl.UpdateOne(flt1, up1);

                    //        var soBld = Builders<SmartplugSwitchedOff_REC>.Filter;
                    //        var soflt = soBld.And(soBld.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))), soBld.Eq<string>("IPAddress", voData.IPAddress));
                    //        var resSo = switchedOff.Find(soflt).SingleOrDefault();

                    //        if (resSo == null)
                    //        {
                    //            SmartplugSwitchedOff_REC poWidget = new SmartplugSwitchedOff_REC();
                    //            poWidget.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                    //            poWidget.DeviceID = resDevice.DeviceID;
                    //            poWidget.DeviceName = resDevice.DeviceName;
                    //            poWidget.IPAddress = resDevice.IPAddress;
                    //            poWidget.Location = resDevice.Location;
                    //            poWidget.On = 1;
                    //            switchedOff.InsertOne(poWidget);
                    //        }
                    //        else
                    //        {
                    //            int newOn = resSo.On + 1;
                    //            var poWidget = Builders<SmartplugSwitchedOff_REC>.Update.Set("On", newOn);
                    //            switchedOff.UpdateOne(soflt, poWidget);
                    //        }
                    //    }
                    //    #endregion
                    //}
                    //else if (voData.Status == "0" || voData.Status == "OFF")
                    //{
                    //    var builder2 = Builders<SmartDevicesView_REC>.Filter;
                    //    var flt2 = builder2.Eq<string>("IPAddress", voData.IPAddress);
                    //    var resDevice = CollectDevice.Find(flt2).SingleOrDefault();

                    //    var statusRelay = resDevice.Relay == 1 ? "ON" : "OFF";
                    //    if (statusRelay != voData.Status)
                    //    {
                    //        var up1 = Builders<SmartDevices_REC>.Update.Set("Relay", status);
                    //        SmartDeviceColl.UpdateOne(flt1, up1);

                    //        #region insert chart switched Off

                    //        var soBld = Builders<SmartplugSwitchedOff_REC>.Filter;
                    //        var soflt = soBld.And(soBld.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))), soBld.Eq<string>("IPAddress", voData.IPAddress));
                    //        var resSo = switchedOff.Find(soflt).SingleOrDefault();

                    //        if (resSo == null)
                    //        {
                    //            SmartplugSwitchedOff_REC poWidget = new SmartplugSwitchedOff_REC();
                    //            poWidget.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                    //            poWidget.DeviceID = resDevice.DeviceID;
                    //            poWidget.DeviceName = resDevice.DeviceName;
                    //            poWidget.IPAddress = resDevice.IPAddress;
                    //            poWidget.Location = resDevice.Location;
                    //            poWidget.Off = 1;
                    //            switchedOff.InsertOne(poWidget);
                    //        }
                    //        else
                    //        {
                    //            int newOff = resSo.Off + 1;
                    //            var poWidget = Builders<SmartplugSwitchedOff_REC>.Update.Set("Off", newOff);
                    //            switchedOff.UpdateOne(soflt, poWidget);
                    //        }
                    //        #endregion
                    //    }
                    //}
                    //else
                    //{
                    //    //var up1 = Builders<SmartDevices_REC>.Update.Set("Relay", 0);
                    //    //SmartDeviceColl.UpdateOne(flt1, up1);
                    //}

                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        private static void setConfig(string message)
        {
            try
            {
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());
                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";

                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("mongodb_server", voobj["mongodb_server"].ToString());
                voResData.Add("hubUrl", voobj["hubUrl"].ToString());
                voResData.Add("MQTTBroker_1", voobj["MQTTBroker_1"].ToString());
                voResData.Add("MQTTBroker_2", voobj["MQTTBroker_2"].ToString());
                voResData.Add("MQTTTopic_1", voobj["MQTTTopic_1"].ToString());
                voResData.Add("MQTTTopic_2", voobj["MQTTTopic_2"].ToString());
                voResData.Add("MQTTBrokerMultiple", voobj["MQTTBrokerMultiple"].ToString());

                // begin write to json file
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                File.WriteAllText(basepatch, strValue);
                // end wriet to json file

                // publish mqtt
                MqttClient_2.Publish(MQTTTopic_2 + "All/setConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                // Restart Apps
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        private static void getConfig(string message)
        {
            try
            {
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());

                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        string mongodb_server = voobj["mongodb_server"].ToString();
                        string signalr_server = voobj["signalr_server"].ToString();
                        string MQTTBroker_1 = voobj["MQTTBroker_1"].ToString();
                        string MQTTBroker_2 = voobj["MQTTBroker_2"].ToString();
                        string MQTTTopic_1 = voobj["MQTTTopic_1"].ToString();
                        string MQTTTopic_2 = voobj["MQTTTopic_2"].ToString();
                        string MQTTBrokerMultiple = voobj["MQTTBrokerMultiple"].ToString();

                        // publish mqtt
                        Dictionary<string, string> voResData = new Dictionary<string, string>();
                        voResData.Add("mongodb_server", mongodb_server);
                        voResData.Add("signalr_server", signalr_server);
                        voResData.Add("MQTTBroker_1", MQTTBroker_1);
                        voResData.Add("MQTTBroker_2", MQTTBroker_2);
                        voResData.Add("MQTTTopic_1", MQTTTopic_1);
                        voResData.Add("MQTTTopic_2", MQTTTopic_2);
                        voResData.Add("MQTTBrokerMultiple", MQTTBrokerMultiple);

                        string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                        MqttClient_2.Publish(MQTTTopic_2 + "All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    }
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        protected override void OnStop()
        {
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "smartplug");
            voResData.Add("msg", "HASIOT - smartplug service is stopped");
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient_2.Publish(MQTTTopic_2 + "All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

        #region snipet
        static void RestartApp(int pid, string applicationName)
        {
            // Wait for the process to terminate
            Process process = null;
            try
            {
                process = Process.GetProcessById(pid);
                process.WaitForExit(1000);
            }
            catch (ArgumentException ex)
            {
                errorHandler(ex.Message);
            }
            Process.Start(applicationName, "");
        }
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        static void errorHandler(string Message)
        {
            // sending error messages
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "All");
            voResData.Add("msg", Message);
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient_2.Publish(MQTTTopic_2 + "All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            // Restart service
            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;
            string applicationName = currentProcess.ProcessName;
            RestartApp(pid, applicationName);
            System.Environment.Exit(1);
        }
        private static void initRTHub()
        {
            try
            {
                //Set connection
                connection = new HubConnection(signalr_server);
                //Make proxy to hub based on hub name on server
                appHub = connection.CreateHubProxy("apphub");
                //Start connection

                connection.Start().ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        task.Exception.GetBaseException();
                    }
                    else
                    {
                        Console.WriteLine("Connected");
                    }
                }).Wait();
            }
            catch (Exception ex)
            {

            }
        }
        private static void heandlerSignalR(string mod, string tp, string sp)
        {
            try
            {
                appHub.Invoke<string>("Send", mod, tp, sp).ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}",
                                          task.Exception.GetBaseException());
                        if (connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                        {
                            initRTHub();
                            heandlerSignalR(mod, tp, sp);
                        }
                    }
                    else
                    {
                        Console.WriteLine(task.Result);
                    }
                });
            }
            catch (Exception ex)
            {
                if (connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                {
                    initRTHub();
                    heandlerSignalR(mod, tp, sp);
                }
            }
        }
        #endregion
    }
}
