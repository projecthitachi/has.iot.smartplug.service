﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace has.iot.smartplug.service
{
    public class SwitchRelay
    {
        public string Status { get; set; }
        public string IPAddress { get; set; }
    }
    public class SmartLog_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string IPAddress { get; set; }
    }
    public class SmartPlugLogView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string IPAddress { get; set; }
    }
    public class SmartChart_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string IPAddress { get; set; }
        public string fullcharger { get; set; }
        public string GadGet { get; set; }
        public decimal TotalToday { get; set; }
        public decimal TotalMonth { get; set; }
        public decimal DailyAvg { get; set; }
        public decimal MonthlyAvg { get; set; }
    }
    public class SmartDevice_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        //public int TotalUsage { get; set; }
    }
    public class SmartDeviceView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string IPAddress { get; set; }
        public int Relay { get; set; }
        public int Status { get; set; }
        public int TotalUsage { get; set; }
    }
    public class SmartWidget_REC
    {
        public smart_id _id { get; set; }
        public decimal ApparentPower { get; set; }
    }
    public class smart_id
    {
        public string DeviceID { get; set; }
    }
    public class SmartPlugLevelUseage_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int recordID { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        public string category { get; set; }
        public int min { get; set; }
        public int max { get; set; }
        public int value { get; set; }
        public string color { get; set; }
        public string ip { get; set; }
        public string zone { get; set; }
        public string personId { get; set; }
    }

    public class SmartplugAllWidgetRealtime_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int recordID { get; set; }
        public string totalUsage { get; set; }
        public string deviceTotal { get; set; }
        public string deviceOn { get; set; }
        public string deviceOff { get; set; }
    }

    public class SmartplugSpecificWidgetRealtime_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public int Voltage { get; set; }
        public int PowerFactor { get; set; }
        public decimal Energy { get; set; }
        public decimal Current { get; set; }
        public decimal ActivePower { get; set; }
        public decimal ApparentPower { get; set; }
        public string IPAddress { get; set; }
        public string fullcharger { get; set; }
        public string GadGet { get; set; }
        public decimal TotalToday { get; set; }
        public decimal TotalMonth { get; set; }
        public decimal DailyAvg { get; set; }
        public decimal MonthlyAvg { get; set; }
    }

    public class PersonLocation
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string gatewayID { get; set; }
        public virtual string type { get; set; }
        public virtual string tagID { get; set; }
        public virtual string rssi { get; set; }
        public virtual string personID { get; set; }
        public virtual string personName { get; set; }
        public virtual string location { get; set; }
        public virtual string defaultZone { get; set; }
        public virtual string avatarID { get; set; }

    }

    public class SmartWidgetLog_REC
    {
        public smart_id _id { get; set; }
        public decimal ApparentPower { get; set; }
        public decimal count { get; set; }
    }
    public class SmartDataWidget_REC
    {
        Random random = new Random();
        public o_date _id { get; set; }
        public DateTime DateTime
        {
            get
            {
                if (_id != null)
                {
                    return Convert.ToDateTime(string.Concat(_id.month, "/", _id.day, "/", _id.year));
                }
                else
                {
                    return DateTime.Now;
                }
            }
            set
            {
                Convert.ToDateTime(String.Concat(_id.month, "/", _id.day, "/", _id.year));
            }
        }
        public string Date
        {
            get
            {
                if (_id != null)
                {
                    return String.Concat(_id.month, "/", _id.day, "/", _id.year);
                }
                else
                {
                    return "";
                }
            }
            set
            {
                String.Concat(_id.month, "/", _id.day, "/", _id.year);
            }
        }
        public int Day
        {
            get
            {
                if (_id != null)
                {
                    return _id.day;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.day.ToString(); }
        }
        public int Month
        {
            get
            {
                if (_id != null)
                {
                    return _id.month;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.month.ToString(); }
        }
        public int Year
        {
            get
            {
                if (_id != null)
                {
                    return _id.year;
                }
                else
                {
                    return 0;
                }
            }
            set { _id.year.ToString(); }
        }
        public string DayName
        {
            get { return (DateTime != null) ? DateTime.ToString("ddd") : ""; }
            set { DateTime.ToString("ddd"); }
        }
        public string MonthName { get { return (DateTime != null) ? DateTime.ToString("MMM") : ""; } set { DateTime.ToString("MMM"); } }
        public string DeviceID
        {
            get { return (_id != null) ? _id.DeviceID : ""; }
            set { _id.DeviceID.ToString(); }
        }
        public double PresentValue
        {
            get { return (_id != null) ? Convert.ToDouble(_id.PresentValue) : 0; }
            set { Convert.ToDouble(_id.PresentValue); }
        }
    }

    public class o_date
    {
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string DeviceID { get; set; }
        public double PresentValue { get; set; }
    }

    public class SmartDeviceData_REC
    {
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public double Consumption { get; set; }
    }

    public class SmartDeviceDataView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public double Consumption { get; set; }
    }

    public class SmartplugDataWidget_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string Location { get; set; }
        public double Consumption { get; set; }
    }

    public class SmartplugDataWidgetRealtime_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string Location { get; set; }
        public double Consumption { get; set; }
    }
    public class SmartplugSwitchedOff_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string Location { get; set; }
        public int On { get; set; }
        public int Off { get; set; }
    }
    public class SmartDevices_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
    }
    public class SmartDevicesView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string ObjectName { get; set; }
        public string Location { get; set; }
        public int SwitchNo { get; set; }
        public int LightController { get; set; }
        public string Category { get; set; }
        public int Relay { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public double Consumption { get; set; }
    }
    public class SmartplugWeekdayConsumtionAvg_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime recordTimestamp { get; set; }
        public string day { get; set; }
        public string unit { get; set; }
        public int Baseline { get; set; }
        public int Current { get; set; }
        public string ip { get; set; }
    }

    public class ConfigurationModel
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public decimal OutDoorTemperature { get; set; }
        public bool OutDoorTemperatureStatus { get; set; }
        public decimal ConstantLux { get; set; }
        public bool ConstantLuxStatus { get; set; }
        public decimal SkinTemperature { get; set; }
        public bool SkinTemperatureStatus { get; set; }
        public decimal HeartRate { get; set; }
        public bool HeartRateStatus { get; set; }
        public int HumanTracing { get; set; }
        public bool HumanTracingStatus { get; set; }
        public string ACMVHumanPresence { get; set; }

    }

    public class SmartlightActiveDevices
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual int RecordStatus { get; set; }
        public virtual string DeviceID { get; set; }
        public virtual string ObjectName { get; set; }
        public virtual int Instance { get; set; }
        public virtual int ObjectTypeID { get; set; }
        public virtual string ObjectTypeName { get; set; }
        public virtual int CategoryID { get; set; }
        public virtual string Category { get; set; }
        public virtual string Plugin { get; set; }
        public virtual string PresentValue { get; set; }
    }

    public class SmartplugDataLogs
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string DeviceID { get; set; }
        public virtual string DeviceName { get; set; }
        public virtual int Voltage { get; set; }
        public virtual int PowerFactor { get; set; }
        public virtual string Energy { get; set; }
        public virtual string Current { get; set; }
        public virtual string ActivePower { get; set; }
        public virtual string ApparentPower { get; set; }
        public virtual string IPAddress { get; set; }
    }

    public class ConfigurationModel2
    {
        [BsonId]
        public ObjectId _id { get; set; }

        public string DefaultVAVTemp { get; set; }
        public string PLUGHumanPresence { get; set; }
        public string PLUGStatus { get; set; }
        public string PLUGLow { get; set; }
        public string PLUGMedium { get; set; }
        public string PLUGHigh { get; set; }

    }
}
